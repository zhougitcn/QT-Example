﻿#ifndef BAIDUSPEECH_H
#define BAIDUSPEECH_H

#include <QObject>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QByteArray>

class BaiduSpeech : public QObject
{
    Q_OBJECT
public:
    explicit BaiduSpeech(QObject *parent, const QString& appkey, const QString& secret);
    void getToken();
    void doASR(const QByteArray data);
    void doTTS(const QString data);
    QString message();
    QByteArray audio();

signals:
    void doneASR();
    void doneTTS();

public slots:
    void on_tokenReply(QNetworkReply* reply);
    void on_ASRReply(QNetworkReply* reply);
    void on_TTSReply(QNetworkReply* reply);

private:
    QNetworkAccessManager* _client;
    QString _appkey;
    QString _secret;
    QString _token;
    QString _msg;
    QByteArray _audio;
};

#endif // BAIDUSPEECH_H
